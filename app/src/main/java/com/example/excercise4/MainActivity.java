package com.example.excercise4;

import android.content.ClipData;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    Toolbar mToolbar;
    TabLayout tbLayout;
    ViewPager vPager;
    RecyclerView myRecyclerView;
    RecyclerView myRecycler;
    List<Residence> mItem = new ArrayList<>();
    recyclerViewAdapter mAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mToolbar = findViewById(R.id.m_toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        vPager = findViewById(R.id.view_pager);
        tbLayout = findViewById(R.id.tab_layout);

//        myRecycler = findViewById(R.id.recycler_view);
//        mAdapter = new recyclerViewAdapter(mItem, this);

        RecyclerView.LayoutManager mLayoutManager = new
                LinearLayoutManager(getApplicationContext());
      //  myRecycler.setLayoutManager(mLayoutManager);
       // myRecycler.setAdapter(mAdapter);

        ViewPagerAdapter adapt = new ViewPagerAdapter(getSupportFragmentManager());
        adapt.addFrg(new frg1(), "مکان های گردشگری");
        adapt.addFrg(new Places(), "اقامتگاه ها");

        vPager.setAdapter(adapt);
        tbLayout.setupWithViewPager(vPager);

        //Code for RecyclerView
       // myRecyclerView = findViewById(R.id.recycler_view);

    }
//    private void setData(){
//        mItem.add(new Residence(2,"Damghan","بلوار شمالی",150000));
//        mItem.add(new Residence(2,"Damghan","بلوار شمالی",150000));
//        mItem.add(new Residence(2,"Damghan","بلوار شمالی",150000));
//        mItem.add(new Residence(2,"Damghan","بلوار شمالی",150000));
//        mItem.add(new Residence(2,"Damghan","بلوار شمالی",150000));
//
//
//    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar, menu);
        return true;
    }
    //Codes For Menu
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.enter) {
            Toast.makeText(getApplicationContext(), "Enter Item Clicked",
                    Toast.LENGTH_SHORT).show();
        }
        else if (id == R.id.register) {
            Toast.makeText(getApplicationContext(), "Register Item Clicked",
                    Toast.LENGTH_SHORT).show();
        }
        else if(id == R.id.call){
            Toast.makeText(getApplicationContext(), "Call Item Clicked",
                    Toast.LENGTH_SHORT).show();
        }
        else if(id == R.id.about){
            Toast.makeText(getApplicationContext(), "About Item Clicked",
                    Toast.LENGTH_SHORT).show();

        }
        else if(id == android.R.id.home){
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
