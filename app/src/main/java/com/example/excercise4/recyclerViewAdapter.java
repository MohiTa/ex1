package com.example.excercise4;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class recyclerViewAdapter extends RecyclerView.Adapter<recyclerViewAdapter.MyviewHolder> {
    List<Residence> residenceList;
    Context mContext;

    public recyclerViewAdapter(List<Residence> residenceList, Fragment fragment) {
        this.residenceList = residenceList;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public MyviewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_residence,parent,false);
        return new MyviewHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull MyviewHolder holder, int position) {
        Residence residence = residenceList.get(position);

        holder.imageView.setImageResource(residence.getAvatar());
        holder.address.setText(residence.getAddress());
        holder.price.setText(residence.getPrice());
        holder.roomNumber.setText(residence.getRoomNumber());
    }

    @Override
    public int getItemCount() {
        return residenceList.size();
    }

    public class MyviewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView;
        public TextView address;
        public TextView price;
        public TextView roomNumber;


        public MyviewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.img_terminal);
            address = itemView.findViewById(R.id.address);
            roomNumber = itemView.findViewById(R.id.roomNumber);
            price = itemView.findViewById(R.id.price);
        }
    }
}